MAINTAINER_NS=my-food
NAME=docker-nodejs
VERSION=10.13
REGISTRY=registry.gitlab.com

files = Dockerfile

build: $(files)
	docker build -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} .

run:
	docker run --rm -i -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} /sbin/my_init -- bash -l

push:
	docker push ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION}
