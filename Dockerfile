# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:0.9.22
MAINTAINER Vojtěch Biberle <vojtech.biberle@gmail.com>

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

RUN set -x \
    && apt-get update \
    && apt-get install -y apt-transport-https apt-utils \
#    && apt-get -y upgrade \
    && curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
    && echo deb https://deb.nodesource.com/node_10.x cosmic main > /etc/apt/sources.list.d/nodesource.list \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y \
        acl \
        git \
        mercurial \
        rsync \
        subversion \
        sudo \
        wget \
        nodejs \
        yarn

# install npm packages
RUN set -x \
    && npm install --global \
        gulp-cli \
        grunt-cli \
        bower \
        markdown-styles \
        npx


ENV WEBAPP_DIR /var/www/website
ENV WEBAPP_USER webapp
ENV WEBAPP_GROUP webapp
ENV WEBAPP_GID 1000
ENV WEBAPP_UID 1000
ENV WEBAPP_PHP_FPM_LISTEN 9000

WORKDIR $WEBAPP_DIR
VOLUME $WEBAPP_DIR


# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Generate czech locales
RUN locale-gen cs_CZ && locale-gen cs_CZ.UTF-8 && update-locale
